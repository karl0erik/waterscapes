====================================================================================
waterscapes: a collection of numerical solvers for cerebral fluid flow and transport
====================================================================================

waterscapes is a collection of Python-based solvers for mathematical
modelling and simulation of the brain's waterscape; i.e. fluid flow
and transport in the brain.

waterscapes is based on the FEniCS Project.

For more information visit http://waterscapes.readthedocs.org

Documentation:
==============

The waterscapes documention is availble on readthedocs, see:

    http://waterscapes.readthedocs.org

License:
========

waterscapes is free software: you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

waterscapes is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program. If not, see
<http://www.gnu.org/licenses/>.
