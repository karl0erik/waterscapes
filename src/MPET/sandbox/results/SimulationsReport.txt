FIXED BRAIN:
If nu = 3.5e-1 we see a very thin boundary layer but the pressure inside is slightly less than the pressure on the boundary.
When nu is very close to 0.5 there is no boundary layer, the pressure is quite damped only in amplitude but not in phase.
For bigger E we see bigger dampening.
Acceleration does not make a difference
    NO ACCCELERATION, FIXED BRAIN:
    
        E = 500
            2D_1Net_PulsatilePressure_FixedBrain_nullspace_False_E_5.000e+02_nu_3.50000e-01_K_1.50000e-05_Q2.222e+09_Donut_coarse_refined/
            2D_1Net_PulsatilePressure_FixedBrain_nullspace_False_E_5.000e+02_nu_3.50000e-01_K_1.50000e-05_Q2.222e+19_Donut_coarse_refined/
            2D_1Net_PulsatilePressure_FixedBrain_nullspace_False_E_5.000e+02_nu_4.99900e-01_K_1.50000e-05_Q2.222e+19_Donut_coarse_refined/
            2D_1Net_PulsatilePressure_FixedBrain_nullspace_False_E_5.000e+02_nu_4.99990e-01_K_1.50000e-05_Q2.222e+09_Donut_coarse_refined/
    
        E = 5000
            2D_1Net_PulsatilePressure_FixedBrain_Acceleration_False_nullspace_False_E_5.000e+03_nu_3.50000e-01_K_1.50000e-05_Q2.222e+09_Donut_coarse_refined
            2D_1Net_PulsatilePressure_FixedBrain_nullspace_False_E_5.000e+03_nu_4.99990e-01_K_1.50000e-05_Q2.222e+09_Donut_coarse_refined/
    
    ACCELERATION, FIXED BRAIN:
        E = 500
            2D_1Net_PulsatilePressure_FixedBrain_Acceleration_nullspace_False_E_5.000e+02_nu_3.50000e-01_K_1.50000e-05_Q2.222e+09_Donut_coarse_refined/
            2D_1Net_PulsatilePressure_FixedBrain_Acceleration_True_nullspace_False_E_5.000e+03_nu_3.50000e-01_K_1.50000e-05_Q2.222e+09_Donut_coarse_refined

FIXED SKULL:
With nu = 0.35 small boundary layer, pressure is almost zero inside.
With nu almost 0.5 we see some dampening and no bl. Anyway, in this case with bigger E we see less dampening.
We see shift in phase as well!


    NO ACCELERATION, FIXED SKULL
    
        2D_1Net_PulsatilePressure_FixedSkull_NoFluxOnVentricles_nullspace_False_E_5.000e+02_nu_4.9999e-01_K_1.500e-05_Q2.222e+09_Donut_coarse/
        2D_1Net_PulsatilePressure_FixedSkull_NoFluxOnVentricles_nullspace_False_E_5.000e+02_nu_4.9999e-01_K_1.500e-05_Q2.222e+09_Donut_coarse_totalpressure/
        2D_1Net_PulsatilePressure_FixedSkull_NoFluxOnVentricles_nullspace_False_E_5.000e+02_nu_4.999e-01_K_1.500e-05_Q2.222e+09_Donut_coarse_totalpressure/
        2D_1Net_PulsatilePressure_FixedSkull_NoFluxOnVentricles_nullspace_False_E_5.000e+03_nu_4.990e-01_K_1.500e-05_Q2.222e+09_Donut_coarse/
        2D_1Net_PulsatilePressure_FixedSkull_NoFluxOnVentricles_nullspace_False_E_5.000e+03_nu_4.9999e-01_K_1.50000e-05_Q2.222e+09_Donut_coarse/
        2D_1Net_PulsatilePressure_FixedSkull_NoFluxOnVentricles_nullspace_False_E_5.840e+02_nu_3.500e-01_K_1.500e-05_Q2.222e+09_Donut_coarse_totalpressure/

